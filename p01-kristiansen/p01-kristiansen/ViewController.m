//
//  ViewController.m
//  p01-kristiansen
//
//  Created by Gustav Kristiansen on 1/31/16.
//  Copyright © 2016 Gus Kristiansen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

bool styleMode;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    styleMode = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonClick:(id)sender{
    styleMode = !styleMode;
    if(styleMode){
        [_label1 setFont: [UIFont fontWithName:@"Zapfino" size:30.0]];
        [_button1 setTitle:@"Deactivate style" forState: UIControlStateNormal];
        UIImage *image = [UIImage imageNamed: @"robot-city-desktop2-1920x1200.jpg"];
        [_imageView1 setImage: image];
    }else{
        [_button1 setTitle:@"Activate style" forState: UIControlStateNormal];
        [_label1 setFont: [UIFont fontWithName:@"System" size:17.0]];
        [_imageView1 setImage: NULL];
    }
}

@end
