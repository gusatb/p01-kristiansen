//
//  ViewController.h
//  p01-kristiansen
//
//  Created by Gustav Kristiansen on 1/31/16.
//  Copyright © 2016 Gus Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *label1;

@property (nonatomic, strong) IBOutlet UIButton *button1;

@property (nonatomic, strong) IBOutlet UIImageView *imageView1;

-(IBAction)buttonClick:(id)sender;

@end
