//
//  AppDelegate.h
//  p01-kristiansen
//
//  Created by Gustav Kristiansen on 1/31/16.
//  Copyright © 2016 Gus Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

