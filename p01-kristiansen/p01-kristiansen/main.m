//
//  main.m
//  p01-kristiansen
//
//  Created by Gustav Kristiansen on 1/31/16.
//  Copyright © 2016 Gus Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
