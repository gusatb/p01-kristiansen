//
//  p01_kristiansenTests.m
//  p01-kristiansenTests
//
//  Created by Gustav Kristiansen on 1/31/16.
//  Copyright © 2016 Gus Kristiansen. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface p01_kristiansenTests : XCTestCase

@end

@implementation p01_kristiansenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
